import angular from 'angular';

import App from './components/app.module';
import appConstants from './constants/app.constants';
import appInterceptors from './interceptors/interceptors.module';
import globalServices from './global-services/global.services.module';
import globalProviders from './providers/global.providers';
import clipboardModule from './vendor/angular-clipboard';
import Raven from 'raven-js';
import RavenAngular from 'raven-js-plugin-angular';

let deps = [
    App.name,
    appConstants.name,
    appInterceptors.name,
    globalServices.name,
    globalProviders.name,
    clipboardModule.name
];

/**
 * Manually bootstrap the application when AngularJS and
 * the application classes have been loaded.
 */
document.addEventListener("deviceready", bootOnDemand, false);

function bootOnDemand () {


    let injector = angular.injector(['ng', globalProviders.name]),
        PlatformStorage = injector.get('PlatformStorage'),
        setupPromise = PlatformStorage.setup();

    setupPromise.then(successSetupStorage, failSetupStorage);

    window.addEventListener('unhandledrejection', onunhandledrejection);

    function onunhandledrejection(evt) {
        Raven.captureException(evt.reason);
    }

    /***
     * Encrypred profile should be now loaded
     * and we can start angular
     */
    function successSetupStorage() {

        Raven
            .config('')
            .addPlugin(RavenAngular, angular)
            .install();

        angular
            .module('app-bootstrap', deps)
            .run(()=>{

                const memberEmail = PlatformStorage.getMemberEmail();

                if(memberEmail) {
                    Raven.setUserContext({
                        id: memberEmail
                    });
                }

            });

        let body = document.getElementsByTagName("body")[0];
        angular.bootstrap(body, ['app-bootstrap']);
    }

    /***
     * Application has crashed and we should do something about it
     * Do not continue if setup fails
     * @param {Exception} error
     */
    function failSetupStorage(error) {
        console.error('Setup failed -> ', error);
    }
}
