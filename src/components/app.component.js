import template from './app.tpl.html';
import AppController from './app.controller';

export const appComponent = {
    template: template,
    controller: AppController,
    controllerAs: 'vm',
    bindings: {}
};