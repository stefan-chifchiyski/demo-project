import translations_en_US from '../translations/en_US.json';
import translations_zh_CN from '../translations/zh_CN.json';
import translations_ja_JPN from '../translations/ja_JPN.json';
import themeGreenSettings from './themes/theme-green.json';
import themeOrangeSettings from './themes/theme-orange.json';
import themeLightSettings from './themes/theme-light.json';
import themeYellowSettings from './themes/theme-yellow.json';

export default configure;

function configure( $mdIconProvider,
                    $mdThemingProvider,
                    $urlRouterProvider,
                    $stateProvider,
                    $locationProvider,
                    $translateProvider,
                    $httpProvider,
                    $logProvider) {

    /**
     * Setup hash bang URL's
     */
    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');

    /**
     * Configure starting URL
     */
    $urlRouterProvider.when('', '/login');
    $urlRouterProvider.otherwise('/login');

    /**
     * Set API interceptors
     */
    $httpProvider.interceptors.push('AppInterceptor');

    /***
     * Reset Angular
     */
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
    $httpProvider.defaults.useXDomain = true;

    /**
     * Configure main abstract state
     */
    $stateProvider.state('app', {
        abstract: true,
        template: '<app></app>'
    });

    /**
     * Configure translations
     */
    $translateProvider.preferredLanguage('en_US');
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

    /**
     * Use the imported JSON for EN-translations
     */
    $translateProvider
        .translations('en_US', translations_en_US)
        .translations('zh_CN', translations_zh_CN)
        .translations('ja_JPN', translations_ja_JPN);

    /**
     * Store translations in localstorage, also remembers the last used language
     */
    $translateProvider.useLocalStorage();

    /***
     * Material design configuration
     */


    // Stops auto themes generation
    $mdThemingProvider.generateThemesOnDemand(true);

    // Create new palettes
    $mdThemingProvider.definePalette('greenPalette', themeGreenSettings.palette);
    $mdThemingProvider.definePalette('orangePalette', themeOrangeSettings.palette);
    $mdThemingProvider.definePalette('lightPalette', themeLightSettings.palette);
    $mdThemingProvider.definePalette('yellowPalette', themeYellowSettings.palette);

    // Set themes options
    $mdThemingProvider.theme('greenTheme')
        .primaryPalette('greenPalette', themeGreenSettings.primaryPalette)
        .accentPalette('greenPalette', themeGreenSettings.accentPallete)
        .warnPalette('greenPalette', themeGreenSettings.warnPalette)
        .backgroundPalette('greenPalette', themeGreenSettings.backgroundPalette);

    $mdThemingProvider.theme('orangeTheme')
        .primaryPalette('orangePalette', themeOrangeSettings.primaryPalette)
        .accentPalette('orangePalette', themeOrangeSettings.accentPallete)
        .warnPalette('orangePalette', themeOrangeSettings.warnPalette)
        .backgroundPalette('orangePalette', themeOrangeSettings.backgroundPalette);

    $mdThemingProvider.theme('lightTheme')
        .primaryPalette('lightPalette', themeLightSettings.primaryPalette)
        .accentPalette('lightPalette', themeLightSettings.accentPallete)
        .warnPalette('lightPalette', themeLightSettings.warnPalette)
        .backgroundPalette('lightPalette', themeLightSettings.backgroundPalette);

    $mdThemingProvider.theme('yellowTheme')
        .primaryPalette('yellowPalette', themeYellowSettings.primaryPalette)
        .accentPalette('yellowPalette', themeYellowSettings.accentPallete)
        .warnPalette('yellowPalette', themeYellowSettings.warnPalette)
        .backgroundPalette('yellowPalette', themeYellowSettings.backgroundPalette);

    // Change default theme (temp)
    $mdThemingProvider.setDefaultTheme('lightTheme');

    if(!localStorage.getItem('defaultTheme')) {

        localStorage.setItem('defaultTheme', 'lightTheme');
    }

    // TO DO: replace value depending on environment build settings
    const debug = !!'{{ DEBUG }}';
    $logProvider.debugEnabled(debug);
}