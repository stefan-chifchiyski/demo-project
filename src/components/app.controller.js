export default AppController;

function AppController ($scope) {

    let vm = this;

    vm.$onInit = activate;

    ////////////////

    function activate () {
        $scope.defaultTheme = localStorage.getItem('defaultTheme') || 'lightTheme';
        $scope.$on('theme:change', updateTheme);
    }

    function updateTheme (e, theme) {
        $scope.defaultTheme = theme;
    }

    ////////////////

}