import angular from 'angular';
import appRun from './app.run';
import configure from './app.config';
import {appComponent} from './app.component';

/**
 * Application entry point
 * Import Angular modules
 */
import angularAnimate from 'angular-animate';
import 'angular-aria';
import ngMaterial from 'angular-material';
import ngSanitize from 'ng-sanitize';
import 'ui-router';
import ngCookies from 'ng-cookies';
import ngStorageModule from 'ng-storage';
import ngResource from 'ng-resource';
import 'ng-translate';
import 'ng-translate-loader-static-files';
import 'ng-translate-storage-local';
import 'ng-translate-storage-cookie';

/**
 * Import Application modules
 */
import {preloaderComponent} from './preloader/preloader.conponent';
import {backButtonComponent} from './back-button/back-button.component';
import {toolbarLoginComponent} from './toolbar-login/toolbar-login.component';
import loginModule from './login/login.module';
import registerModule from './register/register.module';
import passwordRecoveryModule from './password-recovery/password-recovery.module';
import loggedStateModule from './logged-state/logged-state.module';
import wizardModule from './wizard/wizard.module';
import pinModule from './pin/pin.module';
import tfaModule from './tfa/tfa.module';
import onFocusInputRemoveHeaderDirective from './directives/focus.input.remove.header.directive';

/**
 * Define application wide dependencies
 * @type {Array}
 */
let deps = [
    //Vendor
    'ui.router',
    'pascalprecht.translate',
    angularAnimate,
    ngStorageModule.name,
    ngResource,
    ngCookies,
    ngSanitize,
    ngMaterial,
    'ngRaven',

    //App modules
    loginModule.name,
    registerModule.name,
    passwordRecoveryModule.name,
    loggedStateModule.name,
    wizardModule.name,
    pinModule.name,
    tfaModule.name
];

export default angular.module('app', deps)
    .config(configure)
    .component('app', appComponent)
    .component('preloader', preloaderComponent)
    .component('backButton', backButtonComponent)
    .component('toolbarLogin', toolbarLoginComponent)
    .directive('onFocusInputRemoveHeader', onFocusInputRemoveHeaderDirective)
    .run(appRun);