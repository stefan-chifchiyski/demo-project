export default run;

function run($transitions, $log, CurrentSession, PlatformStorage, $mdTheming) {
    $log.info(`Starting the 'app' module`);

    /**
     * On each state change check if the page requires the user to be logged in
     * Pages that requires that a user is logged in, user will redirect to app.login in the feature implementation
     */

    $transitions.onStart({ to: "app.logged.**"}, function(trans) {

        const locked = PlatformStorage.getLockStatus();

        if(locked) {
            return false;
        }else {

            const isLoggedIn = CurrentSession.userSession.isLoggedIn(),
                completedWizard = PlatformStorage.hasCompletedWizard();

            if(isLoggedIn) {

                /***
                 * check for wizard setup complete
                 * And send to wizard state if not completed
                 */
                if(!completedWizard) {

                    return trans.router.stateService.target('app.wizard');
                }

            }else {

                //but the user is not logged in - send to login screen
                return trans.router.stateService.target('app.login');
            }
        }
    });

    $transitions.onStart({to: 'app.*' }, function(trans) {

        const isLoggedIn = CurrentSession.userSession.isLoggedIn();

        /**
         * Prevent logged in users to go to login screen
         */
        if(isLoggedIn && trans.$to().name !== 'app.wizard') {

            return trans.router.stateService.target('app.logged.balance');
        }else if (trans.$to().name === 'app.wizard' && PlatformStorage.hasCompletedWizard()) {
            return false;
        }
    });

    // Generate themes
    $mdTheming.generateTheme('greenTheme');
    $mdTheming.generateTheme('orangeTheme');
    $mdTheming.generateTheme('lightTheme');
    $mdTheming.generateTheme('yellowTheme');

    window.plugins.OneSignal
    .startInit("")
    .endInit();

    if(navigator && navigator.splashscreen){
        navigator.splashscreen.hide();
    }
}