import template from './login.tpl.html';
import LoginController from './login.controller';

export const loginComponent = {
    template: template,
    controller: LoginController,
    controllerAs: 'vm',
    bindings: {}
};