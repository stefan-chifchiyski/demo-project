/**
 * Configure
 * @exports app.login configuration
 */
export default configure;

function configure ($stateProvider) {

    /**
     * Configure State
     */
    $stateProvider.state('app.login', {
        url: '/login',
        views: {
            content: {
                template: '<login></login>'
            }
        },
        data: {
            requireLogin: false
        }
    });
}