import {translations} from './login.translations';

export default LoginController;

function LoginController ($rootScope, $log, $state, AuthService, ProfileService, $translate, NotificationService, Raven) {

    let vm = this;

    vm.$onInit = activate;
    vm.$postLink = function () {
        $rootScope.$broadcast('preloader:hide');
    };


    ////////////////

    function activate () {
        $log.info('Login Controller activated');
        vm.title = 'LoginController';
        vm.showLogin = true;

        translate();

        vm.userData = {};
    }

    /***
     * Check if form valid
     * send request
     */
    vm.initiateLogin = function () {

        if(vm.LoginForm.$valid) {

            $rootScope.$broadcast('preloader:show');

            AuthService.authenticate({
                member_email: vm.userData.member_email,
                member_password: vm.userData.member_password
            }).then(successAuthenticate, failAuthenticate);

        }else {

            vm.LoginForm.$setDirty();

            if (vm.LoginForm.email.$invalid) {

                if (vm.LoginForm.email.$error.email) {

                    NotificationService.show({message: vm.translations['login-input-invalid-email']});

                }else {

                }
            }else if (vm.LoginForm.password.$invalid) {

                if (vm.LoginForm.password.$error.minlength) {

                    NotificationService.show({message: vm.translations['login-input-password-minLength']});

                }else {

                }
            }
        }
    };

    /***
     * Success
     * @param {Object} response
     */
    function successAuthenticate (response) {
        $rootScope.$broadcast('preloader:hide');

        let {successfulLogin, tfaStatus, token} = response;

        if(successfulLogin) {

            Raven.setUserContext({
                id: vm.userData.member_email
            });

            $state.go('app.logged.balance');

        }else {

            if(tfaStatus && token) {

                let code = ProfileService.getAuth();

                //Code present redo login
                if(code) {

                    AuthService.authenticate2FA({
                        member_email: vm.userData.member_email,
                        member_password: token,
                        tfa_token: code
                    }).then(successAuthenticate, failAuthenticate);
                }else {

                    //Show recovery, code not found
                    vm.showLogin = false;
                }
            }else {

                NotificationService.show({
                    message: response.messages
                });
            }
        }
    }

    /***
     * Error
     * @param {Object} response
     */
    function failAuthenticate (response) {
        $log.error(response);

        $rootScope.$broadcast('preloader:hide');

        NotificationService.show({
            message: response.message
        });
    }

    vm.recoverAccount = function () {

        if(vm.RecoveryForm.$valid) {

            ProfileService.setAuth(vm.code).then(function successSetCode () {

                AuthService.authenticateRecover({
                    member_email: vm.userData.member_email,
                    member_password: vm.userData.member_password,
                    tfa_token: vm.code
                }).then(successAuthenticate, failRecoveryAuthenticate);

            }, function errorSetCode () {

                vm.code = '';
                NotificationService.show({
                    message: vm.translations['invalid-recovery-code']
                });
            });

        }else {
            vm.RecoveryForm.$setDirty();
        }
    };

    /***
     * Error - server error recovery validation failed
     * @param {Object} response
     */
    function failRecoveryAuthenticate (response) {
        $log.error(response);

        $rootScope.$broadcast('preloader:hide');

        NotificationService.show({
            message: vm.translations['invalid-recovery-code-server']
        });
    }


    vm.showLoginScreen = function () {
        if(!vm.showLogin) {
            vm.showLogin = true;
        }
    };

    /***
     * Translate translations
     */
    function translate () {

        $translate(translations).then(function assignTranslations (translations) {
            vm.translations = translations;
        });
    }
}