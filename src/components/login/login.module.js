import angular from 'angular';

import {loginComponent} from './login.component';
import sharedComponentsModule from '../logged-state/components/shared.components.module';
import configure from './login.config';

/**
 * Define dependencies
 * @type {Array}
 */
let deps = [sharedComponentsModule.name];

export default angular.module('login', deps)
    .component('login', loginComponent)
    .config(configure);
