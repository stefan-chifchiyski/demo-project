export const translations = [
    'login',
    'email',
    'password',
    'register',
    'cancel',
    'forgot-password',
    'login-title',
    'enter-recovery-code',
    'recover-account',
    'invalid-recovery-code',
    'invalid-recovery-code-server',
    'login-input-required-email',
    'login-input-required-password',
    'login-input-invalid-email',
    'login-input-password-minLength',
    'send-error-address-required'
];