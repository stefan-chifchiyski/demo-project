import template from './password-recovery.tpl.html';
import PasswordRecoveryController from './password-recovery.controller';

export const passwordRecoveryComponent = {
    template: template,
    controller: PasswordRecoveryController,
    controllerAs: 'vm',
    bindings: {}
};