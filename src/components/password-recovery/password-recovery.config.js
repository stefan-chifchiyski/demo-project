/**
 * Configure
 * @exports app.passwordRecovery configuration
 */
export default configure;

function configure ($stateProvider) {

    /**
     * Configure State
     */
    $stateProvider.state('app.passwordRecovery', {
        url: '/password-recovery',
        views: {
            content: {
                template: '<password-recovery></password-recovery>'
            }
        },
        data: {
            requireLogin: false
        }
    });
}