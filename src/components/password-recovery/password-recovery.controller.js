import {translations} from './password-recovery.translations';

export default PasswordRecoveryController;

function PasswordRecoveryController ($rootScope,
                                     $state,
                                     $log,
                                     $timeout,
                                     $translate,
                                     NotificationService,
                                     PasswordRecoveryService,
                                     STATUS_CODES,
                                     AUTHENTICATION) {

    let vm = this;

    vm.$onInit = activate;
    vm.$postLink = function () {

        $timeout(function () {
            $rootScope.$broadcast('preloader:hide');
        }, 500);
    };

    ////////////////

    function activate () {
        $log.info('PasswordRecovery Controller activated');
        vm.title = 'PasswordRecovery Controller';

        vm.userdata = {};

        translate();
    }

    vm.recoverAccount = function () {

        if(vm.PasswordRecoveryForm.$valid){

            $rootScope.$broadcast('preloader:show');

            PasswordRecoveryService.recover({
                user_email: vm.userdata.email,
                client_id: AUTHENTICATION.client_id,
                client_secret: AUTHENTICATION.client_secret,
                reason: 'reset'
            }, successRecoverAccount, failRecoverAccount);

        }else {

            vm.PasswordRecoveryForm.$setDirty();

            if (vm.PasswordRecoveryForm.email.$invalid) {

                if (vm.PasswordRecoveryForm.email.$error.email) {

                    NotificationService.show({message: vm.translations['login-input-invalid-email']});

                }else {

                }
            }
        }
    };

    /***
     * Success
     * @param {Object} response
     */
    function successRecoverAccount (response) {
        $log.info(response);

        $rootScope.$broadcast('preloader:hide');

        if(response.status === STATUS_CODES.SUCCESS) {

            $state.go('app.login');
        }else {

            NotificationService.show({
                message: response.message
            });
        }
    }

    /***
     * Error
     * @param {Object} response
     */
    function failRecoverAccount (response) {
        $log.error(response);

        $rootScope.$broadcast('preloader:hide');

        NotificationService.show({
            message: response.message
        });
    }

    /***
     * Translate translations
     */
    function translate () {

        $translate(translations).then(function assignTranslations (translations) {
            vm.translations = translations;
        });
    }
}