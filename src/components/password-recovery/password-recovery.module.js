import angular from 'angular';

import {passwordRecoveryComponent} from './password-recovery.component';
import configure from './password-recovery.config';
import {PasswordRecoveryService} from './password-recovery.service';

/**
 * Define dependencies
 * @type {Array}
 */
let deps = [];

export default angular.module('passwordRecovery', deps)
    .component('passwordRecovery', passwordRecoveryComponent)
    .factory('PasswordRecoveryService', PasswordRecoveryService)
    .config(configure);
