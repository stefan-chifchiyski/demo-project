export const PasswordRecoveryService = function PasswordRecoveryService ($resource, API, $log)  {

    let params = {},
        mainUrl = API.url + 'member-password-reset-request';

    return $resource(mainUrl, params, {

        recover: {
            method: 'POST',
            isArray: false,
            params: {},
            defaultParams: {

            }
        }

    });
};