export default CurrentSession;

function CurrentSession(LocalStorage) {

    let tokenMutators = {

        accessToken: {
            set: function(value) {
                LocalStorage.set('access_token', value);
            },

            get: function() {
                return LocalStorage.get('access_token');
            },

            delete: function() {
                LocalStorage.remove('access_token');
            }
        },

        refreshToken: {
            set: function(value) {
                LocalStorage.set('refresh_token', value);
            },

            get: function() {
                return LocalStorage.get('refresh_token');
            },

            delete: function() {
                LocalStorage.remove('refresh_token');
            }
        }
    };

    let userSession = {
        isLoggedIn: function() {
            return typeof tokenMutators.accessToken.get() !== 'undefined' && tokenMutators.accessToken.get() !== '';
        }
    };

    return {
        tokens: tokenMutators,
        userSession: userSession
    };
}