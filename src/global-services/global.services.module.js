import angular from 'angular';
import CurrentSession from './currentSession.service';
import AuthModel from './authentication/authentication.model';
import AuthService from './authentication/authentication.service';
import NotificationService from './notification.service'
import DateService from './date.service';
import TFAService from './tfa/tfa.service';
import TFAModel from './tfa/tfa.model';

export default angular.module('global.services', [])
    .service('CurrentSession', CurrentSession)
    .service('AuthModel', AuthModel)
    .service('AuthService', AuthService)
    .service('DateService', DateService)
    .service('TFAService', TFAService)
    .service('TFAModel', TFAModel)
    .factory('NotificationService', NotificationService);