import _ from 'underscore';

export default AppInterceptor;

function AppInterceptor($rootScope, $injector, $q, CurrentSession, STATUS_CODES, $httpParamSerializer) {

    /***
     * Remove after we have means to identify
     * @type {number}
     */
    let temporaryDDOSProtection = 0;

    function request(config) {

        /***
         * Send access token if user is logged in
         * */
        if (CurrentSession.userSession.isLoggedIn()) {
            config.headers['Authorization'] = 'Bearer ' + CurrentSession.tokens.accessToken.get();
        }

        if(!config.headers['Content-Type']) {
            config.headers['Content-Type'] = 'application/json';
        }

        config.headers['Accept'] = 'application/json, text/plain, */*';

        if(config.changeBehaviour) {

            if (config.method.toLowerCase() === 'post') {
                config.transformRequest = function() {
                    return $httpParamSerializer(config.data);
                };
            }
        }

        return config;
    }

    function response(response) {

        temporaryDDOSProtection = 0;

        const status =  response.status || response.realHttpStatus;
        response.data = response.data || {};
        response.data.status = status;

        return response;
    }

    function responseError(rejection) {

        let AuthService = $injector.get('AuthService'),
            CurrentSession = $injector.get('CurrentSession'),
            $http = $injector.get('$http'),
            $rootScope = $injector.get('$rootScope'),
            deferred = $q.defer();

        temporaryDDOSProtection++;

        rejection.data = rejection.data || {};

        if(_.isObject(rejection.data)) {

            if(!rejection.data.status) {
                rejection.data.status = rejection.status;
            }

            if(rejection.data.status === -1) {
                rejection.data.message = 'Connection problem';
            }

        }else if (typeof rejection.data === 'string') {

            rejection.data = {
                html: rejection.data,
                message: 'Server error',
                status: rejection.status
            }
        } else {

            rejection.data = {
                message: 'User must authenticate',
                status: rejection.status
            }
        }

        if(temporaryDDOSProtection > 10) {
            deferred.resolve(rejection);

            return deferred.promise.then(
                function bothTokensDead() {
                    AuthService.logout({force: true});
                }
            );
        }

        /***
         * both tokens are invalid :(\
         * TO DO: check error code and error message
         */
        if (rejection.status === STATUS_CODES.FORBIDDEN && rejection.data.newAuth === true) {

            deferred.resolve(rejection);

            return deferred.promise.then(
                function bothTokensDead() {
                    AuthService.logout({force: true});
                }
            );
        }

        //try to refetch new access token using the refresh token
        if (rejection.status === STATUS_CODES.FORBIDDEN) {

            if (typeof rejection.config.retries === 'undefined') {
                rejection.config.retries = 1;
            }

            if (rejection.config.retries < 10) {

                AuthService.fetchRefreshToken().then(

                    function success(fetchRefreshTokenResult) {

                        if (fetchRefreshTokenResult.successfulTokenFetch === true) {
                            rejection.config.retries = 10;
                            rejection.config.headers['Authorization'] = 'Bearer ' + CurrentSession.tokens.accessToken.get();
                        } else {
                            rejection.config.retries++;
                        }

                        deferred.resolve();
                    },

                    function fail(response) {
                        deferred.reject(rejection);
                    }
                );

            } else {

                /***
                 * if the app has tried to retrieve a new token 10 times and failed
                 */
                deferred.reject(rejection);
            }

            /***
             * this waits for the OPTIONS and POST requests for the new refresh token
             * to pass and then repeats the previous request
             */
            return deferred.promise.then(

                function refreshTokenReceived() {
                    //this repeats the previously issued request
                    return $http(rejection.config);
                },

                function fail() {
                    rejection.config.retries = 10;
                    AuthService.logout({force: true});
                }
            );
        }

        /***
         * Handle server error
         */
        if (rejection.status === STATUS_CODES.SERVER_ERROR) {
            $rootScope.$emit('general:error:show', {
                message: rejection.data.message
            });
        }

        return rejection;
    }

    return {
        request: request,
        response: response,
        responseError: responseError
    };
}