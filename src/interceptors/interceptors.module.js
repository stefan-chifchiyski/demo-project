import AppInterceptor from './interceptor.service';

export default angular
    .module('app.interceptors', [])
    .service('AppInterceptor', AppInterceptor);