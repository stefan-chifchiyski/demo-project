import angular from 'angular';
import ProfileService from './profile.provider';
import LocalStorage from './localStorage.provider';
import PlatformAPIStorage from './device-storage/platform.storage.api.provider';
import PlatformStorage from './device-storage/platform-storage.provider';
import ngCordova from '../vendor/ng-cordova/ng-cordova.module';

export default angular.module('global-providers', [ngCordova.name])
    .provider('LocalStorage', LocalStorage)
    .provider('PlatformAPIStorage', PlatformAPIStorage)
    .provider('PlatformStorage', PlatformStorage)
    .provider('ProfileService', ProfileService);
